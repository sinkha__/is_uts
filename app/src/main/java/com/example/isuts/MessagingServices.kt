package com.example.isuts

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MessagingServices : FirebaseMessagingService() {

    var selamat = ""
    val RC_INTENT = 100
    val CHANNEL_ID = "isuts"

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)

        val intent = Intent(this, MessagingActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        if(p0.getData().size > 0){
            selamat = p0.data.getValue("selamat")

            intent.putExtra("selamat", selamat)
            intent.putExtra("type", 0) //0 = data

            sendNotif("Registrasi Berhasil!!!", intent)
        }

    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun sendNotif(selamat : String, intent: Intent){
        val pendingIntent = PendingIntent.getActivity(this, RC_INTENT, intent,
            PendingIntent.FLAG_ONE_SHOT)

        val ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE)
        val audioAttributes = AudioAttributes.Builder().
        setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE).
        setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build()

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE)
                as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val mChannel = NotificationChannel(CHANNEL_ID, "isuts",
                NotificationManager.IMPORTANCE_HIGH)
            mChannel.description = "isuts"
            mChannel.setSound(ringtoneUri,audioAttributes)
            notificationManager.createNotificationChannel(mChannel)
        }

        val notificationBuilder = NotificationCompat.Builder(baseContext,CHANNEL_ID)
            .setSmallIcon(R.drawable.tipedarah)
            .setLargeIcon(BitmapFactory.decodeResource(resources,R.drawable.tipedarah))
            .setContentTitle(selamat)
            .setSound(ringtoneUri)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true).build()
        notificationManager.notify(RC_INTENT,notificationBuilder)
    }
}