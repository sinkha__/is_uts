package com.example.isuts

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_messaging.*
import java.lang.Exception

class MessagingActivity : AppCompatActivity(), View.OnClickListener {

    var bundle : Bundle? = null
    var topik = "isuts"
    var type = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_messaging)

        FirebaseMessaging.getInstance().subscribeToTopic(topik)
        btnSignin.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener(
            OnCompleteListener { task ->
                if (!task.isSuccessful) return@OnCompleteListener
                edToken.setText(task.result!!.token)
            })

        try {
            bundle = getIntent().getExtras()!!
        }catch (e : Exception) {
            Log.e("BUNDLE", "bundle is null")
        }
        if(bundle != null) {
            type = bundle!!.getInt("type")
            when(type){
                0 -> {
                    edSelamat.setText(bundle!!.getString("selamat"))
                }
            }
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnSignin-> {
                var intent = Intent (this, MainActivity::class.java)
                startActivity(intent)
            }
            }
    }
}