package com.example.isuts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_data.*

class DataActivity : AppCompatActivity(), View.OnClickListener {

    val COLLECTION = "students"
    val F_ID = "id"
    val F_NAME = "name"
    val F_Goldar = "Goldar"
    val F_ADDRESS = "address"
    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var allPendonor : ArrayList<HashMap<String, Any>>
    lateinit var adapter : SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data)

        allPendonor = ArrayList()
        btnUbah.setOnClickListener(this)
        btnTambah.setOnClickListener(this)
        btnHapus.setOnClickListener(this)
        lsData.setOnItemClickListener(itemClick)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if(e !=null ) Log.d("fireStore",e.message)
            showData()
        }
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            allPendonor.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_NAME,doc.get(F_NAME).toString())
                hm.set(F_ADDRESS,doc.get(F_ADDRESS).toString())
                hm.set(F_Goldar,doc.get(F_Goldar).toString())
                allPendonor.add(hm)
            }
            adapter= SimpleAdapter(this, allPendonor,R.layout.row_data,
                arrayOf(F_ID, F_NAME, F_Goldar, F_ADDRESS),
                intArrayOf(R.id.txKode, R.id.txNama, R.id.txGoldar, R.id.txAlamat))
            lsData.adapter=adapter
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambah->{
                val hm = HashMap<String,Any>()
                hm.set(F_ID,edKode.text.toString())
                hm.set(F_NAME,edNama.text.toString())
                hm.set(F_Goldar,edGolDar.text.toString())
                hm.set(F_ADDRESS,edAlamat.text.toString())

                db.collection(COLLECTION).document(edKode.text.toString()).set(hm).
                addOnSuccessListener {
                    Toast.makeText(this,"Data Successfully added",Toast.LENGTH_SHORT).show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this,"Data Unsuccessfully added : ${e.message}",Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btnUbah ->{
                val hm = HashMap<String,Any>()
                hm.set(F_ID,docId)
                hm.set(F_NAME,edNama.text.toString())
                hm.set(F_Goldar,edGolDar.text.toString())
                hm.set(F_ADDRESS,edAlamat.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener {   Toast.makeText(this,"Data Successfully updated",Toast.LENGTH_SHORT)
                        .show() }
                    .addOnFailureListener {  e ->
                        Toast.makeText(this,"Data Unsuccessfully updated : ${e.message}",Toast.LENGTH_SHORT)
                            .show() }
            }
            R.id.btnHapus->{
                db.collection(COLLECTION).whereEqualTo(F_ID,docId).get().addOnSuccessListener {
                        results ->
                    for(doc in results){
                        db.collection(COLLECTION).document(doc.id).delete()
                            .addOnSuccessListener {
                                Toast.makeText(this,"Data Successfully deleted",Toast.LENGTH_SHORT)
                                    .show()
                            }
                            .addOnFailureListener { e ->
                                Toast.makeText(this,"Data Unsuccessfully deleted ${e.message}",Toast.LENGTH_SHORT)
                                    .show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this,"Can't get data's references ${e.message}",Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = allPendonor.get(position)
        docId = hm.get(F_ID).toString()
        edKode.setText(docId)
        edNama.setText(hm.get(F_NAME).toString())
        edGolDar.setText(hm.get(F_Goldar).toString())
        edAlamat.setText(hm.get(F_ADDRESS).toString())
    }

    //menu item//
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option,menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemLogOut ->{
                startActivity(Intent(this, MainActivity::class.java))
                Toast.makeText(baseContext, "Selamat Datang", Toast.LENGTH_SHORT).show()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}